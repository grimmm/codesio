#===========
#Build Stage
#===========
FROM bitwalker/alpine-elixir:1.7 as build
ENV MIX_ENV=prod
RUN apk update && \
    apk add build-base && \
    apk add -u musl musl-dev musl-utils nodejs-npm build-base yarn wget
COPY . .
RUN rm -Rf _build && \
    rm -Rf deps
RUN mix deps.get
RUN cd assets && \
    yarn install && \
    yarn deploy && \
    cd .. && \
    mix phx.digest
RUN mix release
RUN APP_NAME="codesio" && \
    RELEASE_DIR=`ls -d _build/prod/rel/$APP_NAME/releases/*/` && \
    mkdir /export && \
    tar -xf "$RELEASE_DIR/$APP_NAME.tar.gz" -C /export

#================
#Deployment Stage
#================
FROM bitwalker/alpine-erlang:latest
COPY --from=build /export/ .
RUN wget https://s3.amazonaws.com/rds-downloads/rds-ca-2015-root.pem -O /rds-ca-2015-root.pem
ARG port=4000
ARG database_url=postgresql://localhost:5432/codesio_dev
ENV REPLACE_OS_VARS=true \
    PORT=${port} \
    DATABASE_URL=${database_url} \
    ELASTIX_HOST=${elastix_host} \
    SECRET_KEY_BASE=${secret_key_base} \
    USE_SSL=${use_ssl}

EXPOSE 4000

USER default
ENTRYPOINT ["/opt/app/bin/codesio"]
CMD ["foreground"]
